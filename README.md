# Bitbucket.js
Bitbucket.js is a low-level javascript http client for the Bitbucket REST API. It works great in both node and the browser. It allows you to build requests with its promise interface.

## Build Status
![](https://codeship.com/projects/2f810840-21b8-0133-c332-7ee5d26fb714/status?branch=master)

## Requirements
You'll need a newer (v0.12+) version of node with native Promise support or browsers that implement the `Promise` constructor directly. Alternatively, either polyfill `Promise` in the global scope or pass the `Promise` constructor to Bitbucket.js when creating the client.

## API
Require Bitbucket.js using CommonJS or AMD-style `require`. If the environment does not support either of these, bitbucketjs will attach itself as `window.bitbucketjs`.

Bitbucketjs currently only allows login through basic authentication.

Construct a client object like so:

```javascript
var bitbucketjs = require('bitbucketjs');
var bitbucket = bitbucketjs({username: 'myuser', password: 'mypassword'});
```

A number of resources are available through the client, with many actions available that map directly to their REST api equivalents. Bitbucketjs will return to you the response body of the API response.

## `bitbucket.repo`

### bitbucket.repo.fetch('myteam/myrepo')
Returns a `Promise` thenable that resolves to the repository `myrepo` for the team (or user) `myteam`.

This method requires authentication to retrieve private repositories.

### bitbucket.repo.forOwner('myuser')
Returns a `Promise` thenable that resolves to a list of the given user or team's repository.

### bitbucket.repo.create('myteam/myrepo', opts)

**opts**

Object.

`opts.scm` - the version control system of the repo. Can be `git` or `hg`.
`opts.is_private` - `true` for private repositories.
`opts.description` - the repository's description.
`opts.has_issues` - `true` to create this repository with an issue tracker. Defaults to `false`.
`opts.has_wiki` - `true` to create this repository with a wiki. Defaults to `false`.

`opts` accepts all the keys and values that match the parameters of
the [the API repository resource](https://confluence.atlassian.com/display/BITBUCKET/repository+Resource#repositoryResource-POSTanewrepository).


Returns a `Promise` thenable that resolves if the repository was successfully created.

### bitbucket.repo.delete('myteam/myrepo')
Deletes the repository at the given slug. Be careful!

This method requires authentication. Only repositories for which the authenticated user has admin permissions may be deleted.

## `bitbucket.user`

### bitbucket.user.fetch(username)
Returns a `Promise` thenable that resolves to the user of the given `username`.

### bitbucket.user.followers(username)
Returns a `Promise` thenable that resolves to a list of followers of the `username`.

## `bitbucket.team`

### bitbucket.team.fetch(teamname)
Returns a `Promise` thenable that resolves to the team matching `teamname`.

### bitbucket.team.followers(teamname)
Returns a `Promise` thenable that resolves to a list of followers of the `teamname`.

### bitbucket.team.mine(role='member')
Returns a `Promise` thenable that resolves to a list of the authenticated user's teams. `role` can be one of `member`, `admin`, or `contributor`. `role` defaults to `member`.

This method requires authentication.
